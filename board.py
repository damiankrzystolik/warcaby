from square import Square
import random
import re
from functions import clear


class Board:
    def __init__(self):
        self.squares_dict = {}
        self.start_num = 1
        self.player_starting_pawn_spaces = [2, 4, 6, 8, 9, 11, 13, 15, 18, 20, 22, 24]
        self.computer_starting_pawn_spaces = [63, 61, 59, 57, 56, 54, 52, 50, 47, 45, 43, 41]
        self.pl_king_sq = [63, 61, 59, 57]
        self.c_king_sq = [2, 4, 6, 8]

        abc_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        for row in range(1, 9):
            for col in range(1, 9):
                col_letter = abc_list[col - 1]
                s_name = (col_letter + str(row))
                self.square(col, row, s_name)

    def square(self, col, row, name):
        sq = Square(col, row, name, self.start_num)
        number = self.start_num
        self.squares_dict[number] = sq
        self.start_num += 1

    def pawns(self, player_turn):
        pawns = []
        for key in self.squares_dict:
            sq = self.squares_dict[key]
            if player_turn and sq.player:
                pawns.append(key)
            elif not player_turn and sq.pawn and not sq.player:
                pawns.append(key)
        return pawns

    def initial_pawn_positions(self):
        for key in self.squares_dict:
            sq = self.squares_dict[key]
            if sq.start_num in self.player_starting_pawn_spaces:
                sq.player = True
                sq.pawn = True
            if sq.start_num in self.computer_starting_pawn_spaces:
                sq.player = False
                sq.pawn = True

    def check(self):
        for key in self.squares_dict:
            sq = self.squares_dict[key]
            if sq.pawn:
                if sq.player:
                    sq.img = ' ' + chr(128994)  # gracz
                    if sq.king:
                        sq.img = ' ' + chr(129001)

                else:
                    sq.img = ' ' + chr(128992)  # komputer
                    if sq.king:
                        sq.img = ' ' + chr(128999)
            else:
                sq.img = '   ' + sq.name + '  '

    def king_check(self, player_turn):
        for key in self.squares_dict:
            sq = self.squares_dict[key]
            if player_turn:
                if sq.start_num in self.pl_king_sq:
                    if sq.pawn is True and sq.player is True:
                        sq.king = True
            else:
                if sq.start_num in self.c_king_sq:
                    if sq.pawn is True and sq.player is False:
                        sq.king = True

    def print_board(self, player_turn):
        clear()
        self.king_check(player_turn)
        self.check()
        if player_turn is True:
            print('\n*********************  TURA GRACZA  **********************\n')
        elif player_turn is False:
            print('\n*********************  TURA KOMPUTERA  *******************\n')

        print('  |   A      B      C      D       E      F      G      H ')
        print('--+-------------------------------------------------------')
        print('1 |', end='')
        for i in range(1, 9):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('2 |', end='')
        for i in range(9, 17):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('3 |', end='')
        for i in range(17, 25):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('4 |', end='')
        for i in range(25, 33):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('5 |', end='')
        for i in range(33, 41):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('6 |', end='')
        for i in range(41, 49):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('7 |', end='')
        for i in range(49, 57):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')
        print('8 |', end='')
        for i in range(57, 65):
            print(self.squares_dict[i].img.center(6), end='')
        print('\n')

        self.result()

    def result(self):
        number_player_pawn = 0
        number_of_comp_pawn = 0

        for key in self.squares_dict:
            sq = self.squares_dict[key]
            if sq.img == ' ' + chr(128994) or sq.img == ' ' + chr(129001):
                number_player_pawn += 1
            if sq.img == ' ' + chr(128992) or sq.img == ' ' + chr(128999):
                number_of_comp_pawn += 1

        print(f'''Wynik gry:
        G: {12 - number_of_comp_pawn} - K: {12 - number_player_pawn}\n''')


        if number_player_pawn == 0:
            print('Koniec gry. Przegrałes.')
            exit()
        elif number_of_comp_pawn == 0:
            print('Koniec gry. Wygrałes.')
            exit()

    def computer_chooses_pawn(self, player_turn):
        all_pawns = self.pawns(player_turn)
        checked_pawns = set()

        while True:
            if len(checked_pawns) == len(all_pawns):
                print("Komputer nie ma możliwosci ruchu.\nWygrałes.")
                exit()

            pawn = random.choice(all_pawns)
            if pawn in checked_pawns:
                continue

            for key in self.squares_dict:
                square = self.squares_dict[key]
                if square.start_num == pawn and square.pawn and not square.player:
                    possible_moves = self.moves_new(square, player_turn)
                    if possible_moves:
                        n = square.name
                        print('Komputer wybrał pionka ', n)
                        return n

            checked_pawns.add(pawn)

    def dictionary_search(self, obj_name):
        for key in self.squares_dict:
            square = self.squares_dict[key]
            if square.name == obj_name:
                return square

    def pick_up_pawn(self, player_turn):
        while True:
            if player_turn:
                n = str(input('Wybierz pionka > ')).upper()
            else:
                n = self.computer_chooses_pawn(player_turn)

            pattern = re.compile(r'^[A-H]{1}[1-8]{1}$')
            if re.match(pattern, n):
                square = self.dictionary_search(n)
                if square.pawn is True and square.player is player_turn:
                    possible_moves = self.moves_new(square, player_turn)
                    return (possible_moves, n)
                elif square.pawn is True and square.player is not player_turn:
                    print('To pionek przeciwnika. Wybierz inny.')
                elif square.pawn is False:
                    print("To pole jest puste. Wybierz pole z twoim pionkiem.")
            else:
                print("Aby wybrać pole należy wpisać litere kolumny i cyfre rzedu np C4. ")

    def moves_new_for_king(self, pawn):
        moves_list = []
        directions = [(1, 1), (1, -1), (-1, -1), (-1, 1)]
        mapping = {i + 1: chr(65 + i) for i in range(8)}
        for direction in directions:
            pawn_found = False
            for i in range(1, 8):
                if pawn_found:
                    break
                new_col = pawn.col + i * direction[0]
                new_row = pawn.row + i * direction[1]
                try:
                    m = str(mapping[new_col] + str(new_row))
                    for key in self.squares_dict:
                        sq = self.squares_dict[key]
                        if sq.name == m:
                            if sq.pawn:
                                pawn_found = True
                                break
                            else:
                                moves_list.append(m)
                                break
                except KeyError:
                    continue
        return moves_list

    def moves_new(self, pawn, player_turn):
        temp_list = []
        moves_list = []
        mapping = {i + 1: chr(65 + i) for i in range(8)}

        if pawn.king:
            moves_list = self.moves_new_for_king(pawn)
        else:
            new_col = (pawn.col - 1, pawn.col + 1)
            if player_turn:
                new_row = [pawn.row + 1]
            else:
                new_row = [pawn.row - 1]

            for row in new_row:
                if new_col[0] in range(1, 9) and row in range(1, 9):
                    m1 = str(mapping[new_col[0]] + str(row))
                    temp_list.append(m1)
                if new_col[1] in range(1, 9) and row in range(1, 9):
                    m2 = str(mapping[new_col[1]] + str(row))
                    temp_list.append(m2)

        for key in self.squares_dict:
            square = self.squares_dict[key]
            if square.name in temp_list:
                if square.pawn is False:
                    moves_list.append(square.name)

        return moves_list

    def put_down_pawn(self, posible_moves, pawn, player_turn):
        if posible_moves == []:
            return True
        else:
            tmp_bool = False
            while True:
                if player_turn:
                    name = str(input('Wybierz puste pole do położenia pionka lub [P]owtót > ')).upper()
                else:
                    name = random.choice(posible_moves)

                pattern = re.compile(r'^[A-H][1-8]$')
                if name == 'P':
                    break
                elif re.match(pattern, name):
                    if name in posible_moves:
                        for key in self.squares_dict:
                            square = self.squares_dict[key]
                            if square.name == pawn:
                                square.pawn = False
                                square.player = False
                                tmp_bool = square.king
                        for key in self.squares_dict:
                            square = self.squares_dict[key]
                            if square.name == name:
                                square.pawn = True
                                square.player = player_turn
                                square.king = tmp_bool
                                if player_turn is False:
                                    self.print_board(player_turn)
                                    input(f'Wybrano ruch na {square.name}.\nDalej...')
                                return False
                    else:
                        print('Nie uprawniony ruch')

                else:
                    print("Aby wybrać pole należy wpisać litere kolumny i cyfre rzedu np C4. ")

    def mandatory_move(self, flattened_mandatory_capture_moves_list, player_turn):
        tmp_bool = False
        print('Obowiazkowe bicia:')
        print('[Twój pionek] > [Pionek do zbicia] > [Wolne pole]')
        for i, move in enumerate(flattened_mandatory_capture_moves_list, 1):
            print(f"{i}. {move[0]} > {move[1]} > {move[2]}")

        while True:
            if player_turn:
                user_choice = input("Wybierz bicie: ")
                if user_choice.isdigit():
                    index = int(user_choice) - 1
                else:
                    print("Niewłasciwy wybór. Wybierz numer z listy")
                    continue
            else:
                index = random.choice(range(len(flattened_mandatory_capture_moves_list)))
                input(f'Komputer wybrał bicie nr {index + 1}.\nDalej...')

            if 0 <= index < len(flattened_mandatory_capture_moves_list):
                chosen_point = flattened_mandatory_capture_moves_list[index]
                for key in self.squares_dict:
                    square = self.squares_dict[key]
                    if square.name == chosen_point[0]:
                        square.pawn = False
                        square.player = False
                        tmp_bool = square.king
                for key in self.squares_dict:
                    square = self.squares_dict[key]
                    if square.name == chosen_point[1]:
                        square.pawn = False
                        square.player = False
                for key in self.squares_dict:
                    square = self.squares_dict[key]
                    if square.name == chosen_point[2]:
                        square.pawn = True
                        square.player = player_turn
                        square.king = tmp_bool
                        return square
            else:
                print("Niewłasciwy wybór. Wybierz numer z listy")

    def possible_capture(self, pawn, player_turn):
        mandatory_capture_moves = []
        mapping = {i + 1: chr(65 + i) for i in range(8)}

        start_p = str(mapping[pawn.col] + str(pawn.row))

        if pawn.king is False:
            if player_turn:
                directions = [('SE', 1, 1), ('SW', -1, 1)]  #
            else:
                directions = [('NE', 1, -1), ('NW', -1, -1)]
        else:
            directions = [('NE', 1, -1), ('SE', 1, 1), ('NW', -1, -1), ('SW', -1, 1)]

        for direction, col_offset, row_offset in directions:
            path_clear = False
            new_col = pawn.col + col_offset
            new_row = pawn.row + row_offset

            if pawn.king is False:
                if new_col in range(1, 9) and new_row in range(1, 9):
                    m = str(mapping[new_col] + str(new_row))

                    square = self.dictionary_search(m)
                    if square.pawn is True and square.player is not player_turn:
                        new_col_captured = square.col + col_offset
                        new_row_captured = square.row + row_offset
                        if new_col_captured in range(1, 9) and new_row_captured in range(1, 9):
                            m_captured = str(mapping[new_col_captured] + str(new_row_captured))
                            for key in self.squares_dict:
                                sq = self.squares_dict[key]
                                if sq.name == m_captured:
                                    if sq.pawn is False:
                                        some_list = [pawn.name, square.name, sq.name]
                                        mandatory_capture_moves.append(some_list)

            else:
                while new_col in range(1, 9) and new_row in range(1, 9):
                    m = str(mapping[new_col] + str(new_row))
                    square = self.dictionary_search(m)
                    if square.pawn and square.player != player_turn and not path_clear:
                        opp_pawn = square.name
                        path_clear = True
                    elif square.pawn and path_clear:
                        break
                    elif not square.pawn and path_clear:
                        empty_place = str(mapping[new_col] + str(new_row))
                        some_list = [start_p, opp_pawn, empty_place]
                        mandatory_capture_moves.append(some_list)
                    elif square.pawn and square.player == player_turn:
                        break

                    new_col += col_offset
                    new_row += row_offset

        return mandatory_capture_moves