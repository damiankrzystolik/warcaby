from os import system, name


def clear():
    if name == 'nt':
        system('cls')
    else:
        system('clear')
    print('')


def chosen_pawn_posibles_move(posible_moves, player_turn):
    """Funkcja wypisuje możliwe ruchy"""
    if not posible_moves:
        print('Ten pionek nie ma możliwości ruchu.')
    else:
        if player_turn:
            print('Możliwe ruchy: ', end='')
            print(*posible_moves, sep=' ')
        else:
            print('Możliwe ruchy na pola: ', end='')
            print(*posible_moves, sep=' ')
            input('Dalej...')
