from board import Board
from functions import clear, chosen_pawn_posibles_move


class Checkers:
    def __init__(self):
        self.b = Board()
        self.loop = 0
        self.game_turn = True
        self.pawn_in_loop = None

    def game_start(self):
        clear()
        print('''***************  WARCABY  *******************
            _                 _   
           (_)               (_)   
          (___)             (___) 
          _|_|_             _|_|_   
         (_____)           (_____)  
         /_____\\           /_____\\
         ''')
        input("  Rozstawiam pionki i zaczynamy...[ENTER]\n")
        self.b.initial_pawn_positions()

    def play_game(self):
        self.game_start()
        while True:
            self.game_turn = self.play_turn(self.game_turn)

    def play_turn(self, player_turn):
        self.b.print_board(self.game_turn)
        mandatory_capture_moves_list = []
        pawns = self.b.pawns(player_turn)

        if self.pawn_in_loop is None:
            for pawn in pawns:
                mandatory_capture_moves_list.append(self.b.possible_capture(self.b.squares_dict[pawn], player_turn))
        else:
            mandatory_capture_moves_list.append(self.b.possible_capture(self.pawn_in_loop, player_turn))
        mandatory_capture_moves_list = [sub2 for sub1 in mandatory_capture_moves_list for sub2 in sub1]

        if len(mandatory_capture_moves_list) > 0:
            self.pawn_in_loop = self.b.mandatory_move(mandatory_capture_moves_list, player_turn)
            self.loop += 1
            if self.game_turn:
                return True
            else:
                return False

        elif self.loop == 0:
            while True:
                possible = self.b.pick_up_pawn(player_turn)
                chosen_pawn_posibles_move(possible[0], player_turn)
                pdp = self.b.put_down_pawn(possible[0], possible[1], player_turn)
                if pdp is False:
                    self.loop = 0
                    if self.game_turn:
                        return False
                    else:
                        return True


        elif self.loop > 0:
            self.loop = 0
            self.pawn_in_loop = None
            if self.game_turn:
                return False
            else:
                return True


if __name__ == "__main__":
    game = Checkers()
    game.play_game()
