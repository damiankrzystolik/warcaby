class Square:
    """Klasa pojedyńczego obiektu (pola planszy).
    Pojedyńcze pole, które może być puste, z pionkim gracza lub komputera"""
    def __init__(self, col, row, name, start_num):
        self.col = col
        self.row = row
        self.name = name
        self.start_num = start_num
        self.img = None
        self.pawn = False
        self.player = False
        self.king = False

